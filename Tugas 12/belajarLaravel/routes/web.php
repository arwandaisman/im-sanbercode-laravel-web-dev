<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/daftar', [AuthController::class, 'daftar']);
Route::post('/ucapan', [AuthController::class, 'ucapan']);
Route::get('/data-tables',function(){
    return view('page.data-table');
});

Route::get('/table', function(){
    return view('page.table');
});

//testing master template
Route::get('/master', function(){
    return view('layout.master');
});

//CRUD Cast
//Create Data
Route::get('/cast/create',[CastController::class,'create']);
//simpan data
Route::post('/cast',[CastController::class,'store']);

//read data
//tampil semua kategori
Route::get('/cast',[CastController::class,'index']);
//detail data
Route::get('/cast/{id}',[CastController::class,'show']);


//uodate Data
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
//kirim data update
Route::put('/cast/{id}',[CastController::class,'update']);


//delete data
Route::delete('/cast/{id}',[CastController::class,'destroy']);